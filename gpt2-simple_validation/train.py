import gpt_2_simple as gpt2
import os
import requests
import tensorflow as tf
import params
import pdb

params = params.Params()

if not os.path.isdir(os.path.join("models", params.MODEL_NAME)):
	print(f"Downloading {params.MODEL_NAME} model...")
	gpt2.download_gpt2(model_name=params.MODEL_NAME)   # model is

tf.reset_default_graph()
sess = gpt2.start_tf_sess()

try:
    gpt2.load_gpt2(sess, model_name=None)
except FileNotFoundError:
    None

if params.FINE_TUNE:
    gpt2.finetune(sess,
                dataset=params.TRAIN_FILE_NAME,
                model_name=params.MODEL_NAME,
                batch_size=params.BATCH_SIZE,
                sample_every=params.SAMPLE_EVERY,
                steps=params.STEPS,
                pring_every=params.PRINT_EVERY,
                val_dataset=params.TEST_FILE_NAME,
                val_batch_size=2,
                val_batch_count=50,
                val_every=100)
