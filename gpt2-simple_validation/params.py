class Params():
    TRAIN_FILE_NAME = "/home/vbyno/dev/ucu/04_diploma/01_baseline/huggingface/train.txt"
    TEST_FILE_NAME = "/home/vbyno/dev/ucu/04_diploma/01_baseline/huggingface/test.txt"
    MODEL_NAME = "124M"
    # model_name = "355M"
    STEPS = 1000
    FINE_TUNE = True
    BATCH_SIZE = 1
    RUN_NAME = 'run_test'
    SAMPLE_EVERY = 100000
    PRINT_EVERY = 100
