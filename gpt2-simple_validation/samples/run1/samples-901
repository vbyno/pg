======== SAMPLE 1 ========
 fell.

My heart rose in my chest:
it was a kind of happiness, like the love
you had for yourself in childhood but
had not loved for long before
the love you had for yourself only
occurred again in infancy
as it did each time they met and they both moved
like two brothers or three sisters, they both moved
and so there was the fear of death at childhood,
or of being born in it and then dying in it;
the fear of the great loneliness at childhood,
the fear of the mother’s anger at me and her anger
at myself, and when I thought of what was meant
by happiness I had no hope for it, I didn’t understand
the fear of the sorrow in you that it held and the sorrow
inside you where it gripped and twisted under my arms
like a wound of a woman, the sorrow inside you inside your
heart, which inside itself would be a kind of consolation,
but because it was like grief inside my heart felt
like sorrow as well, the sorrow in me which I felt
that it belonged to everything, I tried to be the person in me
by being sad, my whole body and parts of me,
as it is called, I am sad inside, because I’m sad inside
and the life of me as it is called, even though
I am sad inside the life of me, as I’m sad inside the
life of me inside the lives of others, that is
the life of other people. Outside
though I was still not the person who held my
life, there was someone else whose life is
my life, who is not real inside myself or outside the others,
a person whose death is real inside me.
<|endoftext|>
<|context|> Cities & Urban Life, Race & Ethnicity, Social Commentaries
<|title|> The First Race’s The Body’s Belly
<|poem|> From “The Woman at the Door” by
Carolyn Brooks

“In America, the First Race is
The Body’s Belly”
for “Carolyn Brooks”
<|endoftext|>
<|context|> Living, Social Commentaries, Coming of Age, Activities, Life Choices, Relationships, Eating & Drinking, Home Life
<|title|> On Baking in the Town of Dixie
<|poem|> In the cold light, at twilight, the old lady on her porch
wanted to open a pot, so the men and boys
would drink, each half out in a bowl, and then,
the boys, sitting back,
would scoop in their bowls, and while
them all stood drinking,
the old lady in an old hat held a piece
of bread on which was baked two pieces of bread
in the round bowl and held it to be carried by the boys.
Bake the bread two times and the boys would lie down
until the three small pieces melted.
Sometimes a woman would put two or three times her weight
in the bowl, one over the other, and then
the old lady in her hat laid another piece of bread
or two, one over and one over.
Then the boys would get up again.
While the three sat drinking they each took
to the kitchen to put one or two pieces of bread
on the other. With a spoon they lifted all three
to the table. While the babies stood still the boys
suck on their bread with his fingers.
I put the pieces in the bowl. It was dark.
By itself, I didn’t remember. It wasn’t
my cup of tea, and I couldn’t remember it.
But the bread didn’t need no water.
By the time the three boys were asleep,
our grandmother had lifted up her bread,
and it took just as long to reach back.
At sunrise three days a week the boys would lie down
in the yard and eat
until the last supper would be left, and they’d
be done and done the rest of their time together
while the old lady stood on her porch
for two o’clock in the afternoon with a new
piece of bread in a bowl she said would be brought
once a week until the bread was baked.
Then she would bring it to the boys,
and the mothers would take it and go
into the barn and put it into the fire.
I had to stand up and watch the boys.

It was like eating with their mouths open
with my own eyes closed. The men and boys
would try to open the bread like I was a child.
But soon they wouldn’t allow the small pieces of bread

