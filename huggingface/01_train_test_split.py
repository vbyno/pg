from sklearn.model_selection import train_test_split
import pandas as pd
import pdb

TEMPLATE = "<|context|> {0}\n<|title|> {1}\n<|poem|> {2}\n<|endoftext|>\n"

df = pd.read_csv('/home/vbyno/dev/ucu/04_diploma/01_baseline/gpt2-simple/gpt2/poetryfoundation/poems_with_categories.csv')
df = df.loc[df['categories'] != '']

train, test = train_test_split(df, train_size=.95, random_state=42)

print(f"training size: {len(train)}")
print(f"Evaluation size: {len(test)}")

def write_dataset_to_file(dataset, filename):
    def unite_text(d):
        return TEMPLATE.format(d['categories'], d['Title'], d['Content'])
    df_text = pd.DataFrame([])
    df_text['text'] = dataset.apply(unite_text, axis=1)
    df_text['text'].to_csv(f"{filename}.csv")

    with open(f"{filename}.txt", 'w') as file:
        for index, d in df_text.iterrows():
            file.write(d.text)

write_dataset_to_file(test, 'test')
write_dataset_to_file(train, 'train')
