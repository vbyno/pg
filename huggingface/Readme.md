pipenv shell --python 3.7
pipenv install

git clone https://github.com/huggingface/transformers

pipenv install -r transformers/examples/language-modeling/requirements.txt
pipenv install -e ./transformers

```bash
sh 02_train.sh
tensorboard --logdir ./data/runs/
```
