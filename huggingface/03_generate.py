# import tensorflow as tf
# from transformers import TFGPT2LMHeadModel, GPT2Tokenizer
import pdb
import sys
from transformers import GPT2Tokenizer, GPT2LMHeadModel
from transformers import pipeline, set_seed

set_seed(42)

TEMPLATE = "<|context|> {0}\n<|title|> {1}\n<|poem|> {2}"

tokenizer = GPT2Tokenizer.from_pretrained('./output2/')
tokenizer = GPT2Tokenizer.from_pretrained()
model = GPT2LMHeadModel.from_pretrained('./output2/', pad_token_id=tokenizer.eos_token_id)

def get_text():
    context = sys.argv[1]
    title = sys.argv[2]
    poem = sys.argv[3] or ""
    return TEMPLATE.format(context, title, poem)

text = get_text()
# print(text)
input_ids = tokenizer.encode(text, return_tensors='pt')
output = model.generate(input_ids,
                        num_beams=2,
                        max_length=1000,
                        temperature=0.99,
                        no_repeat_ngram_size=2,
                        early_stopping=True)

print(tokenizer.decode(output[0], skip_special_tokens=False))

# output = model(**encoded_input)


# generator = pipeline('text-generation', model=model, tokenizer=tokenizer)

# model = TFGPT2LMHeadModel.from_pretrained("./output", from_pt=True)
# tokenizer = GPT2Tokenizer.from_pretrained("gpt2")


# generator(prefix,  num_return_sequences=2, truncation='<|endoftext|>')

# input_ids = tokenizer.encode(prefix)

# pdb.set_trace()
# generated_text_samples = model.generate(
#     input_ids,
#     max_length=150,
#     num_return_sequences=5,
#     no_repeat_ngram_size=2,
#     repetition_penalty=1.5,
#     top_p=0.92,
#     temperature=.85,
#     do_sample=True,
#     top_k=125,
#     early_stopping=True
# )
