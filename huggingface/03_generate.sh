# /home/vbyno/dev/ucu/04_diploma/01_baseline/huggingface/transformers_local/examples/text-generation/run_generation.py
python "transformers_local/examples/text-generation/run_generation.py" \
    --model_type gpt2 \
    --temperature 0.99 \
    --repetition_penalty 1.0 \
    --length 800 \
    --seed 42 \
    --prompt "<|context|> Living, Philosophy, Sorrow & Grieving\n<|title|>  Poem Written with Issa\n<|poem|> The kids fighting" \
    --stop_token "<|endoftext|>" \
    --model_name_or_path "./data/output_gpt2_3_epochs"
    # --prompt "<|context|> Nature, Animals\n<|title|> Bird Left Behind\n<|poem|> As for her, the circumstances must be ordinary" \
    # --model_name_or_path "./data/output_gptneo_3_epochs"
    # --model_name_or_path "./data/output_distilgpt2_3_epochs"
    # --length 400 \
    # --model_name_or_path "./output2_test2" \
    # --prompt " " \
    # --prefix "<|context|> Nature, Animals\n<|title|> Bird Left Behind\n<|poem|> As for her, the circumstances must be ordinary" \
    # --model_name_or_path="gpt2"
