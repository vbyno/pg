# /home/vbyno/dev/ucu/04_diploma/01_baseline/huggingface/transformers_local/src/transformers/training_args.py
# /home/vbyno/dev/ucu/04_diploma/01_baseline/huggingface/transformers_local/examples/language-modeling/run_clm.py
python transformers_local/examples/language-modeling/run_clm.py \
--model_type "gpt_neo" \
--model_name_or_path "EleutherAI/gpt-neo-125M" \
--output_dir "./data/output_gptneo_3_epochs" \
--num_train_epochs 3 \
--cache_dir "./data/cache" \
--train_file "./train.csv" \
--validation_file "./test.csv" \
--do_train \
--do_eval \
--evaluation_strategy 'epoch' \
--save_strategy "epoch" \
--logging_strategy "epoch" \
--logging_first_step \
--logging_dir "./data/runs/$(date +'%Y_%m_%d_%H_%M')" \
--learning_rate 0.0001 \
--per_device_train_batch_size 2 \
--gradient_accumulation_steps 5 \
--seed 42
# --model_type "xlnet" \
# --model_name_or_path "EleutherAI/gpt-neo-1.3B" \
# --model_name_or_path 'xlnet-base-cased' \
# --output_dir "./data/output_xlnet_base_10_epochs" \
# --overwrite_output_dir \
# --save_steps 1000 \
# --logging_steps 500 \
# --logging_dir "./runs" \
# --fp16 \
# --model_name_or_path "./output2" \
# --max_steps 100 \
# --eval_steps 50 \
