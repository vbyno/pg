import gpt_2_simple as gpt2
import os
import requests
import tensorflow as tf
import params
import pdb
import sys

params = params.Params()
# pdb.set_trace()

sess = gpt2.start_tf_sess()
gpt2.load_gpt2(sess, model_name=None)

print("\n")

TEMPLATE = "<|startoftext|>\n[CONTEXT]: {0}\n[TITLE]: {1}\n[POEM]: {2}"
context = sys.argv[1]
title = sys.argv[2]
poem = sys.argv[3] or ""
prefix = TEMPLATE.format(context, title, poem)
result = gpt2.generate(sess,
            # length=100,
            # prefix="<|startoftext|>",
            truncate="<|endoftext|>",
            prefix=prefix,
            include_prefix=False,
            # nsamples=10,
            # batch_size=10,
            temperature=0.99)
# pdb.set_trace()

sess.close()
