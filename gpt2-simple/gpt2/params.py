class Params():
    FILE_NAME = "./poetryfoundation/poems_with_categories.txt"
    MODEL_NAME = "124M"
    # model_name = "355M"
    STEPS = 20000
    FINE_TUNE = True
    BATCH_SIZE = 1
