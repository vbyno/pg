```bash
scrapy startproject poetryfoundation
```

To start the crawl process, run from the root of this directory:

```bash
scrapy crawl poetry
```

```bash
docker-compose -f docker-compose.mongo.yml exec mongo mongo
use poetryfoundationDB
db.poems.createIndex( { "id": 1 }, { unique: true } )
```

# Run
```bash
scrapy crawl poetryfoundation -a start_page=0 -a count=1400 -a school_period=1951-present -a sort_by=publication_date
```
