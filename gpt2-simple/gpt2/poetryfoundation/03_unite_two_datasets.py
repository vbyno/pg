import pdb
import pandas as pd
import numpy as np
import math
import html
import re

FILE_CATEGORIES = 'poem_categories.csv'
FILE_POEMS = '../../kaggle_poem_dataset.csv'
OUTPUT_FILE = 'poems_with_categories.txt'
OUTPUT_CSV_FILE = 'poems_with_categories.csv'
SEPARATOR = ' | '
NEW_SEPARATOR = ', '
TEMPLATE = "<|startoftext|>\n[CONTEXT]: {0}\n[TITLE]: {1}\n[POEM]: {2}\n<|endoftext|>\n"
TITLE_TEXT = 'Launch Audio in a New Window'

def merge_datasets(file_poems, file_categories):
    df_poems = pd.read_csv(file_poems, encoding = 'utf8')
    df_categories = pd.read_csv(file_categories)

    return df_poems.merge(df_categories,
                          left_on='Poetry Foundation ID',
                          right_on='id',
                          how='left')

def cleanup_categories(categories):
    stop_categories = set([
        'Alliteration', 'Allusion', 'Anaphora', 'Aphorism', 'Ars Poetica',
        'Assonance', 'Aubade', 'Ballad', 'Blank Verse', 'Consonance', 'Couplet',
        'Class', 'Elegy', 'Epigram', 'Epigraph', 'Epistle', 'Epithalamion',
        'Ekphrasis', 'Epic', 'Free Verse', 'Ghazal', 'Haiku', 'Limerick',
        'Metaphor', 'Meters', 'Mixed', 'Nursery Rhymes', 'Ode', 'Ottava Rima',
        'Prose Poem', 'Quatrain', 'Pantoum', 'Pastoral', 'Refrain',
        'Rhymed Stanza', 'Series/Sequence', 'Sestina', 'Simile', 'Sonnet',
        'Stanza Forms', 'Syllabic', 'Symbolist', 'Tercet', 'Terza Rima',
        'Types/Modes', 'Verse Forms', 'Visual Poetry', 'Villanelle'
    ])

    def cleaner(categories):
        return NEW_SEPARATOR.join(set(categories.split(SEPARATOR)) - \
                                  stop_categories)

    return categories.fillna('').apply(cleaner)

def clean_dataset(df):
    def clean_html(text):
        return html.unescape(text)

    def clean_title(text):
        return re.sub("\s\s+", " ", text.replace(TITLE_TEXT, '').rstrip())

    df['Content'] = df['Content'].apply(clean_html)
    df['Title'] = df['Title'].apply(clean_html).apply(clean_title)
    return df

def save_to_file(df, output_file):
    with open(output_file, 'w') as file:
        for index, d in df.loc[df['categories'] != ''].iterrows():
            text = TEMPLATE.format(d['categories'], d['Title'], d['Content'])
            file.write(text)

def save_to_csv_file(df, output_file):
    df.loc[df['categories'] != ''].to_csv(output_file)

if __name__ == '__main__':
    df = merge_datasets(FILE_POEMS, FILE_CATEGORIES)
    df['categories'] = cleanup_categories(df['categories'])
    df = clean_dataset(df)
    save_to_csv_file(df, OUTPUT_CSV_FILE)
    # save_to_file(df, OUTPUT_FILE)
