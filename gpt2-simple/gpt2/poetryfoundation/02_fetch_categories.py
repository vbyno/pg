import json
import pymongo
import pdb
import pandas as pd

FILE = 'poem_categories.csv'
SEPARATOR = ' | '

def transform(poem):
    categories = map(lambda x: x["title"], poem['categories'])

    return {**poem, "categories": SEPARATOR.join(categories)}

if __name__ == "__main__":
    poems_collection = \
        pymongo.MongoClient('localhost', 27017)['poetryfoundationDB']['poems']

    db_poems = poems_collection.find(
            {"categories": { "$ne": []} },
            { "_id": 0, "id": 1, "title": 1, "author": 1, "snippet": 1,
            "categories.title": 1, "school_period": 1 }
        )

    df = pd.DataFrame(map(transform, db_poems))

    df.to_csv(FILE, index=False)
