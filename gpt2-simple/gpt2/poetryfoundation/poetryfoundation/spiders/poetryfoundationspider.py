import scrapy
import pdb
import urllib
import json
import pymongo

class PoetryfoundationSpider(scrapy.Spider):
    name = "poetryfoundation"
    poems_collection = pymongo.MongoClient('localhost', 27017)['poetryfoundationDB']['poems']

    def start_requests(self):
        base_url = "https://www.poetryfoundation.org/ajax/poems"

        start_page = getattr(self, 'start_page', None)
        if not start_page:
            raise TypeError("'start_page' option is not defined. Add -a start_page=40")
        start_page = int(start_page)

        count = getattr(self, 'count', None)
        if not count:
            raise TypeError("'count' option is not defined. Add -a count=100")
        count = int(count)

        valid_sort_by_values=('recently_added', 'publication_date')
        sort_by = getattr(self, 'sort_by', None)
        if sort_by not in valid_sort_by_values:
            raise TypeError("'sort_by' option is not valid. Add -a sort_by=recently_added")

        valid_school_periods=('1951-present')
        self.school_period = getattr(self, 'school_period', None)
        if self.school_period not in valid_school_periods:
            raise TypeError("'school_period' option is not valid. Add -a school_period=1951-present")

        for i in range(start_page, start_page + count):
            params = {
                'page': i,
                'sort_by': sort_by,
                'school-period': self.school_period
            }
            url = f"{base_url}?{urllib.parse.urlencode(params)}"

            yield scrapy.Request(url,
                                 callback=self.parse,
                                 meta=params,
                                 dont_filter=True)

    def parse(self, response):
        body = json.loads(response.body_as_unicode())
        records = map(lambda x: {**x, 'school_period': self.school_period}, body['Entries'])
        self.poems_collection.insert_many(records, ordered=False)
