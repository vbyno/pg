# import libs.transformers_local.examples.language-modeling
import sagemaker
from datasets import load_from_disk

class PoetryGenerator:
    @classmethod
    def train():
        sess = sagemaker.Session(default_bucket='sagemaker-poetry-generation')

        print(f"sagemaker role arn: {role}")
        print(f"sagemaker bucket: {sess.default_bucket()}")

        print(f"sagemaker session region: {sess.boto_region_name}")
        PoetryGenerator
        load_from_disk(args.training_dir)
